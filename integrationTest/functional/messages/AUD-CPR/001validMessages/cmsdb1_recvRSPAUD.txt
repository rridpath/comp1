read SMS800CMSDB1 
   (^0x7e7e7e7e) #msddelim
	#<<< change len below, if needed >>>
   (^148, 4) #overalllen     				
   #{
      (^0x3080)						# SeqOfTagOverall - Sequence-of tag for the overall msg (2)
         (^0x3080)				# SeqOfTagTranHeader - sequence-of tag for the trans hdr (2)
            (^0x020101)                  # Version - version tag (has to be 1) (3)
            (^0x020100)                 # Priority - priority tag (has to be 0) (3)
            (^0x040b)                  # MsgTagLen - msg tag body tag and len (2)
               "^..........."        # MsgTagBody - msg tag body value (11)
            (^0x040e)         # DestNodeNameTagLen - dest node name tag and len (2)
               "^SMS800_CMSDB_1"   # DestNodeName - dest node name value (14)
            (^0x040c)          # SrcNodeNameTagLen - src node name tag and len (2)
               "^STAR_CMSDB_1"      # SrcNodeName - src node name value (12)
            (^0x020100)                  # ErrCode - err code tag, len, and value (3)
            (^0x020113)                  # MsgType - msg code tag, len, and value (3)
         (^0x0000)             # EOCTranHeader - end of 4959 transport hdr wrapper (2)
      (^0x04)                      # MsgOCTTag - UPL and message data octet stream tag (1)
		# <<< change high bit if needed, see note >>>
      (^0x81)                       # LenOfLen - UPL & msg data combine len (1)
                                            # i.e. if the length is greater
                                            # than 255, it will require two
                                            # bytes in the MsgLen field.  In
                                            # that case, LenOfLen should
                                            # become 0x82 (the high bit is a
                                            # flag and not part of the count)
		#<<< change len below, if needed >>>
      (^82,1)                      # ActualLen - 4959 UPL hdr and msg data (1)
      #{
         (^0x3080)             # SequenceOfTag - Sequence of for UPL header & msg data (2)
            (^0x04)             # UPLHeaderTag - UPL header tag (1)
            (^0x1b)             # UPLHeaderLen - UPL header len (always 27) (1)
            #{
               "^0"                # ConfFlag - Confirmation flag (1)
               "^.........."        # CorelID - Correlation id (10)
               "^STAR_CMSDB_1"  # SrcNodeName - source node name (12)
               "^RCA"               # RouteID - destination routing code (3)
               "^ "              # UPLErrCode - error code (1)
            #}
            (^0x04)                # MsgLen798 - start of msg data octet stream (1)
				# <<< change high bit if needed, see note >>>
            (^0x81)                 # LenOfLen - len-of-len (1)
				#<<< change len below, if needed >>>
            (^46,1)             # ActualLen798 - <<< change >>> (1)
            #{
               "^RSP-AUD:,"         # VerbMod - RSP-AUD response back (9)
               "^$YYYY-$MM-$DD,"       # date - Date (11)
               "^..-..-..-..."         # time - Time includes...for time zone (12)
               "^:::"                # colons - (3)
               "^COMPLD,00:"        # ErrCode - Error code for normal record (10)
               (^0x3B)           # TermSemiCol - terminating semicolon (1)
            #}
         (^0x0000)      # EndUPLMsg - end of 4959 UPL/message data wrapper (2)
      #}
      (^0x0000) ;  # EOCOverall - EOC tag end the overall msg sequence-of (2)
   #}
