REM Enable all foreign key constraints in the current schema

set serveroutput on size 1000000

DECLARE
  CURSOR curCons IS
    SELECT table_name, constraint_name
    FROM user_constraints
    WHERE constraint_type='R'
      AND status='DISABLED'
    ORDER BY table_name, constraint_name;
BEGIN
  FOR recCons IN curCons LOOP
    dbms_output.put_line('Enabling constraint: ' || recCons.table_name || '.' || recCons.constraint_name);
    BEGIN
      EXECUTE IMMEDIATE 'alter table ' || recCons.table_name || ' enable constraint "' ||
                        recCons.constraint_name || '"';
    EXCEPTION 
      WHEN OTHERS THEN
        dbms_output.put_line('Error: ' || SQLERRM);
    END;
  END LOOP;
END;
/
