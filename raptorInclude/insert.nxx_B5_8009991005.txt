# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = NXX 
# Branches = 5 
# AN type = 128 or Dialed Number
#**********************************
#CPR Top Decision Node - NPA 2 branches
#**********************************
#Decision Node 1: 1
#Qualifiers: 0
##Nbr of Branches: 2
#Branch 1 Values: 1
#	Value: 303
#Branch 2 Values: 0
#
#**********************************
#CPR Child 1 Node - NXX 5 branches
#**********************************
#Decision DN1 B1:
#	Decision Node 1: 8
#	Qualifiers: 0
#	Nbr of Branches: 4
#	Branch 1 Values: 1
#		Value: 111
#	Branch 2 Values: 1
#		Value: 222
#	Branch 3 Values: 1
#		Value: 333
#	Branch 4 Values: 1
#		Value: 444
#	Branch 5 Values: 0
#
#Action DN1 B2: 128/3039991000
#
#**********************************
#CPR Child 2 Node
#**********************************
#Action DN2 B1: 128/3039998221
#Action DN2 B2: 128/3039998222
#Action DN2 B3: 128/3039998223
#Action DN2 B4: 128/3039998224
#Action DN2 B5: 128/3039998000


# Variable used for CPR and CRN tables 
var CPR_ID  = '91005' ;

# Load data into CPR table 
var CPR  = '0100000200000013000101012F0000004100000800000500000049000101006F0000005100010100DE00000059000101014D0000006100010101BC00000069000080012F03E703E8FF80012F03E7201DFF80012F03E7201EFF80012F03E7201FFF80012F03E72020FF80012F03E71F40FF' ;
var SHA1  = 'npanxx 5 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991005' ;
scenario CRNTable.insert.txt ;
