#############################################################################
# Verify that there is a single row in the CRN table
# Use this script after database has been truncated 
#############################################################################

# Note: use "XcelleratorDBA --describe ALL" to see the latest table layout.

var COUNT = '0' ;
scenario VerifyCRNTableCountWithSubVar.txt ;

fg XcelleratorDBA --count ALL > count.out ;
grepit  count.out "CALL_ROUTING_NUMBER count\s+:\s+0\s+"; # 0 row is expected
