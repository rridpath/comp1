#**********************************
#Message Attributes
#**********************************
#CRN: $crnUpdUcr
#Eff Date (YYYYMMDDQQ): $effDateUpdUcr
#Resp Org: $respOrgUpdUcr
#SLR: $slrUpdUcr
#SLT: $sltUpdUcr
#Correlation ID: $correlationID
#
#**********************************
#CPR Top Decision Node
#**********************************
#Decision Node 1: 6
#Qualifiers: 0
#Nbr of Branches: 3
#Branch 1 Values: 1
#       Value: 100
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3039990001

# Variable used for CPR and CRN tables
var CPR_ID  = '91003' ;

# Load data into CPR table 
var CPR  =
'060000010000000C0001016480012F03E70001FF'
;

var SHA1  = 'percent 5 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991008' ;
scenario CRNTable.insert.txt ;



