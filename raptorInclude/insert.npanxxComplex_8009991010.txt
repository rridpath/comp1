# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = 3 branch NPA with 3 branch NXXs with multiple values 
#**********************************
#CPR Top Decision Node - NPA 3 branch
#**********************************
#Decision Node 1: 1
#Qualifiers: 0
#Nbr of Branches: 3
#Branch 1 Values: 1
#	Value: 301
#Branch 2 Values: 1
#	Value: 302
#Branch 3 Values: 0
#
#**********************************
#CPR Child 1 Node - NXX 3 branches
#**********************************
#Decision DN1 B1:
#	Decision Node 2: 8
#	Qualifiers: 0
#	Nbr of Branches: 3
#	Branch 1 Values: 3 
#		Value: 111
#		Value: 112
#		Value: 113
#	Branch 2 Values: 1
#		Value: 121
#	Branch 3 Values: 0
#
#Decision DN1 B2:
#	Decision Node 3: 8
#	Qualifiers: 0
#	Nbr of Branches: 3
#	Branch 1 Values: 1
#		Value: 211
#	Branch 2 Values: 2 
#		Value: 212
#		Value: 213
#	Branch 3 Values: 0
#
#Action DN1 B3: 128/3038888888
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN2 B1: 128/3011118111
#Action DN2 B2: 128/3011128111
#Action DN2 B3: 128/3018888888
#Action DN3 B1: 128/3022118111
#Action DN3 B2: 128/3022128111
#Action DN3 B3: 128/3028888888

# Variable used for CPR and CRN tables 
var CPR_ID  = '91010' ;

# Load data into CPR table 
var CPR  = '010000030000001C000101012D0000003E000101012E0000005D00000800000300000065000301006F0100700100710000006D0001010079000000750000080000030000007D00010100D30000008500020100D40100D50000008D000080012F037822B8FF80012D006F1FAFFF80012D00701FAFFF80012D037822B8FF80012E00D31FAFFF80012E00D41FAFFF80012E037822B8FF' ;
var SHA1  = '3B NPA with 3B NXXs' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991010' ;
scenario CRNTable.insert.txt ;
