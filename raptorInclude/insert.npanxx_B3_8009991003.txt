# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = NPANXX 
#**********************************
#CPR Top Decision Node - NPANXX 3 branches
#**********************************
#Decision Node 1: 9
#Qualifiers: 0
#Nbr of Branches: 3
#Branch 1 Values: 1
#	Value: 303100
#Branch 2 Values: 1
#	Value: 303200
#Branch 3 Values: 0
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3039999100
#Action DN1 B2: 128/3039999200
#Action DN1 B3: 128/3039999000

# Variable used for CPR and CRN tables 
var CPR_ID = '91003' ;

# Load data into CPR table 
var CPR = '0900000300000020000101012F006400000028000101012F00C800000030000080012F03E7238CFF80012F03E723F0FF80012F03E72328FF' ;
var SHA1 = 'npanxx 3 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN = '8009991003' ;
scenario CRNTable.insert.txt ;
