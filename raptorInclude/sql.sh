#! /bin/sh

# --- $0 <options> sql_statement
# --- options:
# ---   -c sql_credentials (default: $SQL_CRED)

CRED=${SQL_CRED}

while getopts ":c:" option
do
  case $option in
    c)  
      CRED=$OPTARG
      ;;  
    \?) 
      echo "Invalid option: -$OPTARG" >&2 
      exit 1
      ;;  
    :)  
      echo "Option -$OPTARG requires an argument." >&2 
      exit 1
      ;;  
  esac
done
shift $(($OPTIND - 1))

if [ -z "$CRED" ]; then
  echo "SQL credentials (option -c) not specified" >&2 
  exit 1
fi

echo "$1;" | sqlplus -S $CRED
