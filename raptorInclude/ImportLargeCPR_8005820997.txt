# there is a foreign key constraint so call_processing_record has
# to be populated first
fg echo -e "insert into call_processing_record (
   select * from star.call_processing_record
   where cpr_id=120899) \; \n
   exit\n"
   | sqlplus -S $DB_USER/$DB_PASS@$SID ;

# import CPRs associated with the same records that were created
# in the call_processing_record import
fg echo -e "insert into call_routing_number values (
   (select crn from star.call_routing_number where cpr_id=120899),
   120899, '          ', 'ROR12', 0, 0) \;\n
   exit\n"
   | sqlplus -S $DB_USER/$DB_PASS@$SID ;
