# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
# WARNING!!!!  This CPR is BAD because it has day of year 400 
# Use only for NEGATIVE testing

# DN = DOY (DOR in spec)  
# Branches = 2 
# AN type = 128 or Dialed Number
#**********************************
#CPR Top Decision Node
#**********************************
#Decision Node 1: 3
#Qualifiers: 2
#Qualifier 1: 2/2
#Qualifier 2: 3/2
#Nbr of Branches: 2
#Branch 1 Values: 1 
#	Value: 344  # Modified to be 190 or "day" 400
#Branch 2 Values: 0
#
#**********************************
#CPR Child 2 Node
#**********************************
#Action DN1 B1: 128/3031003111
#Action DN1 B2: 128/3031003888


# Variable used for CPR and CRN tables 
var CPR_ID  = '91005' ;

# Load data into CPR table 
var CPR  = '03020204030200020000001700010101900000001F000080012F00640C27FF80012F00640F30FF' ;
var SHA1  = 'BAD DOY of 400' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991005' ;
scenario CRNTable.insert.txt ;
