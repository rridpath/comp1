#**********************************
#Message Attributes
#**********************************
#CRN: $crnUpdUcr
#Eff Date (YYYYMMDDQQ): $effDateUpdUcr
#Resp Org: $respOrgUpdUcr
#SLR: $slrUpdUcr
#SLT: $sltUpdUcr
#Correlation ID: $correlationID
#
#**********************************
#CPR Top Decision Node
#**********************************
#Decision Node 2: 5
#Qualifiers: 2
#Qualifier 1: 2/4
#Qualifier 2: 3/2
#Nbr of Branches: 8
#Branch 1 Values: 1
#       Range: 0-12
#Branch 2 Values: 1
#       Range: 12-24
#Branch 3 Values: 1
#       Range: 24-36
#Branch 4 Values: 1
#       Range: 36-48
#Branch 5 Values: 1
#       Range: 48-60
#Branch 6 Values: 1
#       Range: 60-72
#Branch 7 Values: 1
#       Range: 72-84
#Branch 8 Values: 0
#
#**********************************
#CPR Child 2 Node
#**********************************
#Action DN1 B1: 128/3039053001
#Action DN1 B2: 128/3039053002
#Action DN1 B3: 128/3039053003
#Action DN1 B4: 128/3039053004
#Action DN1 B5: 128/3039053005
#Action DN1 B6: 128/3039053006
#Action DN1 B7: 128/3039053007
#Action DN1 B8: 128/3039053008

# Variable used for CPR and CRN tables
var CPR_ID  = '91003' ;

# Load data into CPR table 
var CPR  =
'05020204030200080000004D000102000C000000550001020C180000005D00010218240000006500010224300000006D000102303C000000750001023C480000007D000102485400000085000080012F03890BB9FF80012F03890BBAFF80012F03890BBBFF80012F03890BBCFF80012F03890BBDFF80012F03890BBEFF80012F03890BBFFF80012F03890BC0FF'
;

var SHA1  = 'time 8 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991008' ;
scenario CRNTable.insert.txt ;

