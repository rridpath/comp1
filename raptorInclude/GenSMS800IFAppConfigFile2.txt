# Create the configuration file.  It is named Sms800IFConfig2.out so git will ignore
# it, but in fact it is an input Sms800IFConfig file for a command below.
fg echo "DB_USER=$DB_USER \# The app uses this to connect to the DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "DB_PASSWORD=$DB_PASS \# The app uses this to connect to the DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "DB_CONNECTSTRING=$DB_SID \# The app uses this to connect to the DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "DB_NAME=unused \# This value is not used by Oracle" >> $parentDir/Sms800IFConfig2.out ;
fg echo "ipc=$SMS8002_IF_AT_PORT_SUB_VAR \# The AdminTool port." >> $parentDir/Sms800IFConfig2.out ;
#---- first NODE IP address and port ----
fg echo "SMS800_NODE_IF_IP_ADDR_0=0.0.0.0 \# The first SMS800 NODE IP address." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_NODE_IF_IP_PORT_0=$SMS8002_IF_NODE_0_SERVER_PORT_SUB_VAR \# The first SMS800 NODE port." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CLIENT_NODE_0_NAME=STA2_NODE_0 \# The source node name used when interacting with the first NODE port of the SMS800 server." >> $parentDir/Sms800IFConfig2.out ;
#---- second NODE IP address and port ----
fg echo "SMS800_NODE_IF_IP_ADDR_1=0.0.0.0 \# The second SMS800 NODE IP address." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_NODE_IF_IP_PORT_1=$SMS8002_IF_NODE_1_SERVER_PORT_SUB_VAR \# The first SMS800 NODE port." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CLIENT_NODE_1_NAME=STA2_NODE_1 \# The source node name used when interacting with the second NODE port of the SMS800 server." >> $parentDir/Sms800IFConfig2.out ;
#---- first CMSDB IP address and port ----
fg echo "SMS800_CMSDB_IF_IP_ADDR_0=0.0.0.0 \# The first SMS800 CMSDB IP address." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CMSDB_IF_IP_PORT_0=$SMS8002_IF_CMSDB_0_SERVER_PORT_SUB_VAR \# The first SMS800 CMSDB port." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CLIENT_CMSDB_0_NAME=STA2_CMSDB_0 \# The source node name used when interacting with the first CMSDB port of the SMS800 server." >> $parentDir/Sms800IFConfig2.out ;
#---- second CMSDB IP address and port ----
fg echo "SMS800_CMSDB_IF_IP_ADDR_1=0.0.0.0 \# The second SMS800 CMSDB IP address." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CMSDB_IF_IP_PORT_1=$SMS8002_IF_CMSDB_1_SERVER_PORT_SUB_VAR \# The second SMS800 CMSDB port." >> $parentDir/Sms800IFConfig2.out ;
fg echo "SMS800_CLIENT_CMSDB_1_NAME=STA2_CMSDB_1 \# The source node name used when interacting with the second CMSDB port of the SMS800 server." >> $parentDir/Sms800IFConfig2.out ;
#---- retry timer for socket reconnect attempts
fg echo "AF_SOCKET_REOPEN_TIMER=5 \# Interval in seconds between socket reopen attempts." >> $parentDir/Sms800IFConfig2.out;

#---- software release version number
fg echo "VERSION_NUMBER=$SOFTWARE_RELEASE_VERSION_NUMBER_SUB_VAR \# The Software Release version number." >> $parentDir/Sms800IFConfig2.out ;

#---- message interface identifier
fg echo "MSG_INTERFACE_ID=$MESSAGE_INTERFACE_IDENTIFIER_SUB_VAR \# The Specification Message Interface Identifier." >> $parentDir/Sms800IFConfig2.out ;

#---- Sms server name
fg echo "SERVER_NAME=SMS2 \# This is the sms server name" >> $parentDir/Sms800IFConfig2.out ;

fg echo "TEST_FLAG=0,i \# This is a flag used only for TESTING!" >> $parentDir/Sms800IFConfig2.out;
fg echo "IGNORE_REPLICATE_MISMATCH=false,s \# This is a flag used only for TESTING!" >> $parentDir/Sms800IFConfig2.out;

fg echo "CR1_USER=$DB_USER \# The app uses this to connect to the call router 1 DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "CR1_PASSWORD=$DB_PASS \# The app uses this to connect to the call router 1 DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "CR1_CONNECTSTRING=$DB_SID \# The app uses this to connect to the call router 1 DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "CR2_USER=$DB_USER \# The app uses this to connect to the call router 2 DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "CR2_PASSWORD=$DB_PASS \# The app uses this to connect to the call router 2 DB" >> $parentDir/Sms800IFConfig2.out ;
fg echo "CR2_CONNECTSTRING=$DB_SID \# The app uses this to connect to the call router 2 DB" >> $parentDir/Sms800IFConfig2.out ;
