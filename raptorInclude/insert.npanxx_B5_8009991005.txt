# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = NPANXX 
#**********************************
#CPR Top Decision Node - NPANXX 5 branches
#**********************************
#Decision Node 1: 9
#Qualifiers: 0
#Nbr of Branches: 5
#Branch 1 Values: 1
#	Value: 303100
#Branch 2 Values: 1
#	Value: 303200
#Branch 3 Values: 1
#	Value: 303300
#Branch 4 Values: 1
#	Value: 303400
#Branch 5 Values: 0
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3039999100
#Action DN1 B2: 128/3039999200
#Action DN1 B3: 128/3039999300
#Action DN1 B4: 128/3039999400
#Action DN1 B5: 128/3039999000

# Variable used for CPR and CRN tables 
var CPR_ID = '91005' ;

# Load data into CPR table 
var CPR = '0900000500000036000101012F00640000003E000101012F00C800000046000101012F012C0000004E000101012F019000000056000080012F03E7238CFF80012F03E723F0FF80012F03E72454FF80012F03E724B8FF80012F03E72328FF' ;
var SHA1 = 'npanxx 5 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN = '8009991005' ;
scenario CRNTable.insert.txt ;
