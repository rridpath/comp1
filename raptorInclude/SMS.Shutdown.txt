#
#  This script will issue the command to shutdown the SMS Interface
#  and wait for it to be usre it exits correctly.
#
# send shutdown command to CallRouter using Admin Tool
sleep 1 ;
print Shutting Down the SMS800 Interface.;

fg admin --dest $SMS800_IF_AT_PORT --cmd "shutdown" 2>&1 >> $parentDir/admin.out ;

# Block for shutdown related messages.  Then it's safe for the next test.
grepit try 40 sleep 0.250 $parentDir/Sms800IF.log   "The App has shutdown." ;

