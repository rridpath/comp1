# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = ANI
# Branches = 2
# AN type = 128 or Dialed Number
#**********************************
# CPR Top Decision Node
#**********************************
# Decision Node 1: 1
# Qualifiers: 0
# Nbr of Branches: 2
# Branch 1 Values: 1
#	Value: 303
# Branch 2 Values: 0
#
#**********************************
# CPR Child 1 Node
#**********************************
# Action DN1 B1: 128/3039732222
# Action DN1 B2: 128/3039732000

# Variable used for CPR and CRN tables 
var CPR_ID  = '91002' ;

# Load data into CPR table 
#var CPR  = '0100000200000013000101012F0000001B000080012F03CD08AEFF00000000000000FF' ; # this is the valid CPR
var CPR  = '0100000200000013000101012F0000001B00000000000000000EFF80012F03CD07D0FF' ; # this is CPR with 0's to produce error
var SHA1  = 'ANI simple 2 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991002' ;
scenario CRNTable.insert.txt ;
