#! /bin/sh
### NOTE
# THIS FILE IS THE SAME IN raptorInclude and testInclude IF YOU MODIFY DO SO
# IN BOTH PLACES!!
### NOTE

# --- execute a sql command

# --- parameters : sql statement to be executed
# --- return code: 0 for success, else 1
# ---
# --- cInspector : run cmd sql-execute.sh "truncate table mytable";
# ---
# --- Note       : see sql.sh for how to supply sql credentials

statement=$1
shift

# Get the user from the env
TMP=/tmp/$(basename $0).$$.$USER
$(dirname $0)/sql.sh $* "$statement" > $TMP

grep ERROR $TMP > /dev/null
if [ 0 = $? ]; then
  cat $TMP >&2
  rm $TMP
  exit 1
fi

rm $TMP
