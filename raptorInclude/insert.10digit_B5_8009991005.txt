# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = 10 digit 
#**********************************
#CPR Top Decision Node - 10 digit 5 branches
#**********************************
#Decision Node 1: 10 
#Qualifiers: 0
#Nbr of Branches: 5
#Branch 1 Values: 1
#	Value: 3031111111
#Branch 2 Values: 1
#	Value: 3032222222
#Branch 3 Values: 1
#	Value: 3033333333
#Branch 4 Values: 1
#	Value: 3034444444
#Branch 5 Values: 0
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3039999111
#Action DN1 B2: 128/3039999222
#Action DN1 B3: 128/3039999333
#Action DN1 B4: 128/3039999444
#Action DN1 B5: 128/3039999000

# Variable used for CPR and CRN tables 
var CPR_ID  = '91005' ;

# Load data into CPR table 
var CPR  = '0A0000050000003E000101012F006F045700000046000101012F00DE08AE0000004E000101012F014D0D0500000056000101012F01BC115C0000005E000080012F03E72397FF80012F03E72406FF80012F03E72475FF80012F03E724E4FF80012F03E72328FF' ;
var SHA1  = '10 digit 5 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991005' ;
scenario CRNTable.insert.txt ;
