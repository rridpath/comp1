# Create the configuration file.  It is named config.out so git will ignore
# it, but in fact it is an input config file for a command below.
fg echo "DB_USER=$DB_USER \# The app uses this to connect to the DB" >> $parentDir/config2.out ;
fg echo "DB_PASSWORD=$DB_PASS \# The app uses this to connect to the DB" >> $parentDir/config2.out ;
fg echo "DB_CONNECTSTRING=$DB_SID \# The app uses this to connect to the DB" >> $parentDir/config2.out ;
fg echo "DB_NAME=unused \# This value is not used by Oracle" >> $parentDir/config2.out ;
fg echo "DB_BACKUP_CONNECTSTRING=$DB_SID \# The app uses this to connect to the backup billing DB" >> $parentDir/config2.out ;
fg echo "ipc=$CR2_AT_PORT \# The AdminTool port." >> $parentDir/config2.out ;
fg echo "CALL_ROUTER_SIP_SERVER_PORT=$CALL_ROUTER2_SIP_SERVER_PORT_VAR \# The port that Call Router listens on for sip request" >> $parentDir/config2.out ;
fg echo "SERVER_NAME=ROUTER1 \# This is the server name in the routing server table" >> $parentDir/config2.out ;
fg echo "LOCAL_HB_TS_OFFSET_SECONDS=0 \# If > 0, hb retrieved from router db is offset by this many seconds to cause alarm" >> $parentDir/config2.out ;

