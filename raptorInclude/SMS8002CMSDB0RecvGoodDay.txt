read SMS8002CMSDB0 
(0x7e7e7e7e)                  # 4959 message delimiter
(0x00000033)                  # overall message length - 51 bytes
   (0x3080)                   # Sequence-of tag for the overall message
      (0x3080)                # sequence-of tag for the transport header
         (0x020101)           # version 1, 1 is the only value allowed
         (0x020100)           # priority 00 is the only valude allowed
         (0x040b)             # message tag tag and length
            "..........."  	# message tag body (fixed length 11 chars)
         (0x0400)             # empty dest node name (len==00 'who are you')
         (0x040C)             # source node name tag and length
            "STA2_CMSDB_0"  # source node name
         (0x020100)           # error code 0=no error, 1=bad source
                                 # 2=bad destination, 3=bad version
         (0x020101)           # message type/'code' (Good Day) 01
      (0x0000)                # EOC tag terminating the transport header sequence-of
      (0x0400)                # empty UPL header/message body
   (0x0000)                   # EOC tag terminating the overall message sequence-of 
   ;
