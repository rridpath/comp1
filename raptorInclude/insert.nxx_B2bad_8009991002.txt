# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = NXX 
# Branches = 2
# AN type = 128 or Dialed Number
#**********************************
#CPR Child 1 Node - NXX 2 branches
#**********************************
#Decision DN1 B1:
#	Decision Node 1: 8
#	Qualifiers: 0
#	Nbr of Branches: 2
#	Branch 1 Values: 1
#		Value: 111
#	Branch 2 Values: 0
#
#Action DN1 B2: 128/3039991000
#
#**********************************
#CPR Child 2 Node
#**********************************
#Action DN2 B1: 128/3039998222
#Action DN2 B2: 128/3039998000

# Variable used for CPR and CRN tables 
var CPR_ID  = '91002' ;

# Load data into CPR table 
var CPR  = '0100000200000027000101012F000000260000080000020000002E000101006F00000036000080012F03E703E8FF80012F03E7201EFF80012F03E71F40FF' ;
var SHA1  = 'npanxx 2 branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991002' ;
scenario CRNTable.insert.txt ;
