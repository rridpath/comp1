# Before the sockets are re-used by raptor (for a new test)
# you should close them.  This way, when the sockets are re-opened, they
# are done so from a clean slate. 
#
# This is also a good way to confirm the Sms800IF exited.
close SMS8002CMSDB0 ;
close SMS8002CMSDB1 ;
close SMS8002NODE0 ;
close SMS8002NODE1 ;
