#############################################################################
# Update a single row in the table.
#############################################################################

# Note: use "XcelleratorDBA --describe ALL" to see the latest table layout.

#
# Here's an example of how to invoke this script:
#
# var FIELD=configName ;
# var VALUE=configValue ;
# scenario $REPO/testInclude/ConfigTable.update.txt ;
#

fg /bin/echo -e "UPDATE CONFIG SET value = '$VALUE' WHERE field = '$FIELD' \;\n"
   | sqlplus -S $DB_USER/$DB_PASS@$SID ;
