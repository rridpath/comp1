#############################################################################
# Add a single row to the table.  Call this in a loop for many rows.
#############################################################################

# Note: use "XcelleratorDBA --describe ALL" to see the latest table layout.

# These have to be populated so this has to be set by the test calling this
# SERVER_NAME
# SID_VALUE                
# SERVER_IP
# SERVER_IPC_PORT
# PRIMARY_FLAG    

# This will default here so the insert will work, for those tests that
# really care about these values, these should be defined with that test.
var default  BILLING_BACKUP_ADDRESS = 0.0.0.0 ;
var default  BILLING_BACKUP_PORT    = 0 ;
var default  PRIMARY_SMS_NAME       = SMS1 ;
var default  PRIMARY_SMS_ADDRESS    = 0.0.0.0 ;
var default  PRIMARY_SMS_PORT       = 0 ;
var default  SECONDARY_SMS_NAME     = SMS2 ;
var default  SECONDARY_SMS_ADDRESS  = 0.0.0.0 ;
var default  SECONDARY_SMS_PORT     = 0 ;
var default  SID                    = $DB_SID ;

fg /bin/echo -e " INSERT INTO ROUTING_SERVER_TABLE VALUES
   (
      '$SERVER_NAME',
      '$SID_VALUE',
      '$SERVER_IP',
      '$SERVER_IPC_PORT',
      '$PRIMARY_FLAG',
      '$BILLING_BACKUP_ADDRESS',
      '$BILLING_BACKUP_PORT',
      '$PRIMARY_SMS_NAME',
      '$PRIMARY_SMS_ADDRESS',
      '$PRIMARY_SMS_PORT',
      '$SECONDARY_SMS_NAME',
      '$SECONDARY_SMS_ADDRESS',
      '$SECONDARY_SMS_PORT'
      ) \;\n "
   | sqlplus -S $DB_USER/$DB_PASS@$SID  ;
