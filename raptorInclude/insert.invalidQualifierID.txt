# WARNING!!!! This is an INVALID CPR use with caution!!!
#**********************************
#Message Attributes
#**********************************
#CRN: $crnUpdUcr
#Eff Date (YYYYMMDDQQ): $effDateUpdUcr
#Resp Org: $respOrgUpdUcr
#SLR: $slrUpdUcr
#SLT: $sltUpdUcr
#Correlation ID: $correlationID
#
#**********************************
#CPR Top Decision Node
#**********************************
#Decision Node 2: 5
#Qualifiers: 2
#Qualifier 1: 2/4  
#Qualifier 2: 3/2  NOTE:  Made this invalid by making the 3 an 8
#Nbr of Branches: 2
#Branch 1 Values: 1
#       Range: 0-0
#Branch 2 Values: 0
#
#**********************************
#CPR Child 2 Node
#**********************************
#Action DN1 B1: 128/3039053001
#Action DN1 B2: 128/3039053002

# Variable used for CPR and CRN tables
var CPR_ID = '91003' ;

# Load data into CPR table 
#var CPR = '05020204030200020000001700010200000000001F000080012F03890BB9FF80012F03890BBAFF' ;
var CPR = '05020204080200020000001700010200000000001F000080012F03890BB9FF80012F03890BBAFF' ;

var SHA1 = 'invalid Qualifier ID' ;  
scenario $REPO/raptorInclude/CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN = '8009991002' ;
scenario $REPO/raptorInclude/CRNTable.insert.txt ;



