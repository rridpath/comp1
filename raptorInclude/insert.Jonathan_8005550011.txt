# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = Demo	 
# Branches = 2
# AN type = 128 or Dialed Number
#**********************************
# CPR Top Decision Node
#**********************************
#**********************************
#CPR Top Decision Node - Jonathan single branch
#**********************************
#Decision Node 1: 10
#Qualifiers: 0
#Nbr of Branches: 2
#Branch 1 Values: 1
#	Value: 8005550011
#Branch 2 Values: 0
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3135550011
#Action DN1 B2: 128/3135550011

scenario raptorEnvironment.txt;

#var DB_USER = 'star';

# Variable used for CPR and CRN tables 
var CPR_ID  = '5550011' ;

# Load data into CPR table 
var CPR  = '0A000002000000170001010320022B000B0000001F0000800139022B000BFF800139022B000BFF' ;
var SHA1  = 'Jonathan CPR' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8005550011' ;
scenario CRNTable.insert.txt ;
