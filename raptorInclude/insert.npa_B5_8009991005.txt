# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = NPA 
# Branches = 3 - with one branches containing 2 branch values 
# AN type = 128 or Dialed Number
#**********************************
#CPR Top Decision Node - NPA 5 branch
#**********************************
#Decision Node 1: 1
#Qualifiers: 0
#Nbr of Branches: 5
#Branch 1 Values: 4
#          Value: 110
#          Value: 115
#          Value: 120
#          Value: 125
#Branch 2 Values: 3
#          Value: 130
#          Value: 135
#          Value: 140
#Branch 3 Values: 2
#          Value: 150
#          Value: 155
#Branch 4 Values: 1
#          Value: 160
#Branch 5 Values: 0
#
#**********************************
#CPR Child 1 Node - NXX 3 branches
#**********************************
#Decision DN1 B1:
#      Decision Node 2: 8
#      Qualifiers: 0
#      Nbr of Branches: 3
#      Branch 1 Values: 1
#         Value: 001
#      Branch 2 Values: 1
#         Value: 002
#      Branch 3 Values: 0
#
#Decision DN1 B2:
#      Decision Node 3: 8
#      Qualifiers: 0
#      Nbr of Branches: 3
#      Branch 1 Values: 1
#         Value: 001
#      Branch 2 Values: 1
#         Value: 002
#      Branch 3 Values: 0
#
#Decision DN1 B3:
#      Decision Node 4: 8
#      Qualifiers: 0
#      Nbr of Branches: 3
#      Branch 1 Values: 1
#         Value: 001
#      Branch 2 Values: 1
#         Value: 002
#      Branch 3 Values: 0
#
#Decision DN1 B4:
#      Decision Node 5: 8
#      Qualifiers: 0
#      Nbr of Branches: 3
#      Branch 1 Values: 1
#         Value: 001
#      Branch 2 Values: 1
#         Value: 002
#      Branch 3 Values: 0
#
#Action DN1 B5: 128/9062259999
#
#********************************
#CPR Child 2 Node
#********************************
#Action DN2 B1: 128/9062250001
#Action DN2 B2: 128/9062250002
#Action DN2 B3: 128/9062250222
#Action DN3 B1: 128/9062250001
#Action DN3 B2: 128/9062250002
#Action DN3 B3: 128/9062250333
#Action DN4 B1: 128/9062250001
#Action DN4 B2: 128/9062250002
#Action DN4 B3: 128/9062250444
#Action DN5 B1: 128/9062250001
#Action DN5 B2: 128/9062250002
#Action DN5 B3: 128/9062250555

# Variable used for CPR and CRN tables
var CPR_ID  = '91003' ;

# Load data into CPR table 
var CPR  = '0100000500000040000401006E01007301007801007D0000005C000301008201008701008C00000078000201009601009B0000009400010100A0000000B0000008000003000000B80001010001000000C00001010002000000C8000008000003000000D00001010001000000D80001010002000000E0000008000003000000E80001010001000000F00001010002000000F800000800000300000100000101000100000108000101000200000110000080038A00E1270FFF80038A00E10001FF80038A00E10002FF80038A00E100DEFF80038A00E10001FF80038A00E10002FF80038A00E1014DFF80038A00E10001FF80038A00E10002FF80038A00E101BCFF80038A00E10001FF80038A00E10002FF80038A00E1022BFF' ;
var SHA1  = 'NPA 3 branch with multi value branch' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8009991005' ;
scenario CRNTable.insert.txt ;
