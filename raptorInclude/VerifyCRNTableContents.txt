#############################################################################
# NOTE:  ALL variables MUST be used for the script to execute otherwise FAIL 
# CAUTION: Be aware large CPRs can be VERY ugly so wise to use small to med
# Verify contents of the CRN table
# Verify CPR in CRN table
#############################################################################

# Note: use "XcelleratorDBA --describe ALL" to see the latest table layout.

#
# Here's an example of how to invoke this script:
#
# var CRN = 'crnValue' ; # Call Routing Number 
# var EFD = 'efdValue' ; # Effective Date
# var ROR = 'rorValue' ; # Responsible Organization
# var SLR = 'slrValue' ; # Sampling Rate
# var SLT = 'sltValue' ; # Sampling Type
# var CPR = 'cprValue' ; # Call Processing Record 
# scenario VerifyCRNTableContents.txt ;
#

# Validate CRN Table
fg /bin/echo -e " SELECT * FROM CALL_ROUTING_NUMBER WHERE CRN = $CRN \n \; \n " 
| sqlplus -S $DB_USER/$DB_PASS@$SID > crntableval.out;


grepit crntableval.out "$CRN\s" ;
grepit crntableval.out "$EFD\s" ;
grepit crntableval.out "$ROR\s" ;
grepit crntableval.out "$SLR\s" ;
grepit crntableval.out "$SLT\s" ;


# Validate Call Processing Record Table 
fg /bin/echo -e "SELECT CPR FROM CALL_PROCESSING_RECORD WHERE CPR_ID = (SELECT CPR_ID FROM CALL_ROUTING_NUMBER WHERE CRN = $CRN) \;"
 | sqlplus -S $DB_USER/$DB_PASS@$SID > cprval.out;

grepit cprval.out "$CPR\s+" ; # expected value passed in

