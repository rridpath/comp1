# The following will load data into the following tables:
# CPR - Call Processing Record
# CRN - Call Routing Number tables
#  
# DN = Demo	 
# Branches = 2
# AN type = 128 or Dialed Number
#**********************************
# CPR Top Decision Node
#**********************************
#**********************************
#CPR Top Decision Node - Ronnie single branch
#**********************************
#Decision Node 1: 10
#Qualifiers: 0
#Nbr of Branches: 2
#Branch 1 Values: 1
#	Value: 8005550016
#Branch 2 Values: 0
#
#**********************************
#CPR Child 1 Node
#**********************************
#Action DN1 B1: 128/3135550016
#Action DN1 B2: 128/3135550016

scenario raptorEnvironment.txt;

#var DB_USER = 'star';

# Variable used for CPR and CRN tables 
var CPR_ID  = '5550016' ;

# Load data into CPR table 
var CPR  = '0A000002000000170001010320022B00100000001F0000800139022B0010FF800139022B0010FF' ;
var SHA1  = 'Ronnie CPR' ;  
scenario CPRTable.insert.txt ;

# Load data into the CRN Table
var CRN  = '8005550016' ;
scenario CRNTable.insert.txt ;
