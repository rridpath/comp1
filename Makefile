DIRECTORIES = src/install/linux

SUBMODULES = RapTor2.0 

findme = $(if $(realpath $(1)),$(or $(realpath $(1)/$(2)),$(call findme,$(1)/..,$(2))))
MAKEFILE_COMMON := $(call findme,$(CURDIR),buildEnv/include/Makefile.common)
include $(MAKEFILE_COMMON)

