Summary: comp1
Name: comp1
Version: %VERSION%
Release:  %RELEASE%
Group:Applications
License: Commercial
BuildRoot: /tmp/%USER%/build-root
Prefix: /opt/10x


%description
comp1 submodule

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp1
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp1/bin
mkdir -p $RPM_BUILD_ROOT/opt/10x/comp1/config

# Executables
cp %PROJECT_BASE/src/files/file1 $RPM_BUILD_ROOT/opt/10x/comp1/bin
cp %PROJECT_BASE/src/files/file2 $RPM_BUILD_ROOT/opt/10x/comp1/bin

# Config Files
cp %PROJECT_BASE/src/config/comp1.config $RPM_BUILD_ROOT/opt/10x/comp1/config
cp %PROJECT_BASE/src/config/broncos.config $RPM_BUILD_ROOT/opt/10x/comp1/config


%files
%defattr(755,root,root,755)
/opt/10x/comp1/bin
/opt/10x/comp1/config
